




//this program crash when amount length is greater than 12, fix it

#include <stdio.h>
#include <string.h>
#include <ctype.h>
int findIndexOfDot(const char * dataIn, int len){
    int index = 0;
    while(isdigit(dataIn[index]) != 0 && index < len){
        index++;
    }
    return index;
}

void zeroPadAmount(const char * amountIn, int len, char * paddedOut){
    char  paddedTemp[13] = {0};
    char  padded[13] = {0};
    int filledIndex = 0;
    int rem = 0;
    int indexOfDot = findIndexOfDot(amountIn, len);
    strncpy(paddedTemp, amountIn, indexOfDot);

    filledIndex = strlen(paddedTemp);
    strncpy(paddedTemp + filledIndex, amountIn + filledIndex + 1, len - filledIndex);

    filledIndex = strlen(paddedTemp);
    rem = 12 - filledIndex;
    memset(padded, '0', rem);
    strcpy(padded + rem, paddedTemp);

    strcpy(paddedOut, padded);
}

int main(){
    //this program crash when amount length is greater than 12, fix it
    const char * amount = "20000000";
    char padded[15] = {0};

    zeroPadAmount(amount, strlen(amount), padded);
    printf("Padded is %s\n", padded);
}