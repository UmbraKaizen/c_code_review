// Type your code here, or load an example.
#include <stdio.h>
#include <string.h>

int getIndexOfNonZero(const char* dataIn, int len){
    int index = 0;
    while(dataIn[index] == '0' && index < len){
        index++;
    }
    return index;
}


void formatAmount(const char * amountIn, int len, char * output){
    char amountTemp[20] = {0};
    char koboArea[5] = {0};
    char nairaArea[18] = {0};
    int indexOfNonZero = getIndexOfNonZero(amountIn, len);
    strcpy(amountTemp, amountIn + indexOfNonZero);
    
    int amountLen = strlen(amountTemp);
    strncpy(nairaArea, amountTemp, amountLen - 2);
   // snprintf(koboArea, 3, "%s", amountTemp + strlen(nairaArea));
    strcpy(koboArea, amountTemp + strlen(nairaArea));
    
    sprintf(output, "%s.%02s", nairaArea, koboArea);



}

int main(){
    const char * amount = "000000002450";
    char output[20] = {0};
    formatAmount(amount, strlen(amount), output);
    printf("Result is %s\n", output);
}