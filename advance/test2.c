// Type your code here, or load an example.
#include <stdio.h>
#include <string.h>

int getIndexOfNonZero(const char* dataIn, int len){
    int index = 0;
    while(dataIn[index] == '0' && index < len){
        index++;
    }
    return index;
}


void formatAmount(const char * amountIn, int len, char * output){



}

int main(){
    const char * amount = "000000002450";
    
    //Amount is a formated representation of naira and kobo(Naira.Kobo e.g 24.50) 
    //with the decimal indicator removed and padded with zero to make the length of the string 12
    //padding is done by prefixing the amount information with enough zeros to make the string length equal to 12
    // e.g 24.50 becomes 000000002450 , 240.50 becomes 000000024050 etc.
    // you are to implement the formatAmount function to extract the naira and kobo part
    // and save the normal naira and kobo representation to output, like 24.50
    char output[20] = {0};
    formatAmount(amount, strlen(amount), output);
    printf("Result is %s\n", output);
}